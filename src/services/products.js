import axios from "axios";

export const getProducts = async () => {
   axios
     .get('https://fakestoreapi.com/products')
     .then((response) => {
       setData(response.data);
       console.log(response.data);
     })
     .catch((error) => {
       console.error(error);
     });
 };