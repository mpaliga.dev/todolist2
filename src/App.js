import './App.css';

import { ListOfProducts } from './components/ListOfProducts.js';
import { TodoWrapper } from './components/TodoWrapper.js';
import { Route, Routes } from 'react-router-dom';
import { Toggler } from './components/buttonToggle.js';
import { NavBar } from './components/NavBar.js';


function App() {
  return (
    <>
    < NavBar />
    <div>

      <Routes>      
        {/* <Route path="/todo" element={<TodoWrapper />} /> */}
        <Route path="/products" element={<ListOfProducts />} />
        <Route path="/" element= {< TodoWrapper />} />
      </Routes>
      </div>
      </>
  );
}

export default App;
