import React from "react";
import { NavLink } from "react-router-dom";
import styled from 'styled-components';

import "../App.css"


const NavUnlisted = styled.ul`

  display: flex;

  a {
    text-decoration: none;
  }

  li {
    color: olive;
    background-color: darkkhaki;
    margin: 0 0.8rem;
    font-size: 1.3rem;
    position: relative;
    list-style: none;
    padding: 0.5em;
  }

  .current {
    li {
      border-bottom: 2px solid black;
    }
  }
`;

export const NavBar= () => {
    return (
       <NavUnlisted>
          <NavLink to="/" className='todo' exact><li>Todo</li> </NavLink>
          <NavLink to="/products"  className='products' exact><li>Products</li> </NavLink>
       </NavUnlisted> 
      );
}
