import React, {useState} from "react";
import { TodoWrapper } from "./TodoWrapper";
import { ListOfProducts } from "./ListOfProducts";




export const Toggler= () => {
    const [toggle, setToggle] = useState(false)

    return (
      <div>
        {toggle ? (
          <div>
            <button onClick={() => setToggle(false)}>Products</button>
            <div>
              <TodoWrapper />
            </div>
          </div>
        ) : (
          <div>
            <button onClick={() => setToggle(true)}>Todo List</button>
            <div>
              <ListOfProducts />
            </div>
          </div>
        )}
      </div>
    );
};