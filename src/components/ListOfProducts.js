import React, { useEffect, useState } from "react";
import axios from "axios";

export const ListOfProducts = () => {
  const [data, setData] = useState(null);

  const getProducts = async () => {
    axios
      .get('https://fakestoreapi.com/products')
      .then((response) => {
        setData(response.data);
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
  getProducts()
  }, []); 

  return (
    <div>
      <h2 style={{ padding: '30px',backgroundColor: "azure"  }} >List of Products</h2>
      <div className="product-list">
        {data ? (
          data.map(product => (
            <div key={product.id} className="productCard" >
              <h3 style={{backgroundColor: "azure"}}>{product.title}</h3>
            </div>

          ))
        ) : (
          <p>Loading...</p>
        )}
      </div>
    </div>
  );
};
